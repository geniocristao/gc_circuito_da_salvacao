# GC_Circuito_da_salvacao

Atividade eletrônica para ensino da trindade e do processo de salvação cristã

//=== CHANGELOG ===//
/*
 
 ## [18.10.1] - 2020-06-17
 
 ### ADICIONADO
 - Adicionado o delay para retardar a inicialização da placa de rede
 
 ### CORRIGIDO:
 - Aumentado o tamanho do array de dados "vcEnvio" recebidos do i2c, pois isso
   ocaciona o reset da placa de rede
 
 ### MODIFICADO:
 - Refatorado o processo de conexão com o servidor mqtt criando variaveis
 
*/ 
